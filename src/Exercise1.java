import java.util.ArrayList;
import java.util.List;

public class Exercise1 {
    public static void main(String[] args) {
        Postman postman = new Postman(20);
        Person person = postman;
        System.out.println(postman.getAge() == person.getAge());
        List<Job> jobs = new ArrayList<>();
        jobs.add(postman);
        for (Job job: jobs) {
            job.doJob();
        }
    }
}

class Postman extends Person implements Job {
    public Postman(int age) {
        super(age);
    }

    public void doJob() {
        System.out.println("You've got mail!");
    }

    @Override
    public void talk(String message) {
        System.out.println("Postman: " + message);
    }
}

abstract class Person {
    private int age;
    public Person(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }
    public abstract void talk(String message);
}

interface Job {
    void doJob();
}