public class Exercise7 {
    public void testOutput() {
        Person p = new Person("Joe", "Bloggs");
        System.out.println("Person details: " + p); // Prints "Person details: Test"
    }

    public static void main(String[] args) {
        new Exercise7().testOutput();
    }
    class Person {
        private String firstName;
        private String surname;

        public Person(String firstName, String surname) {
            this.firstName = firstName;
            this.surname = surname;
        }

        @Override
        public String toString() {
            return "Test";
        }
    }
}