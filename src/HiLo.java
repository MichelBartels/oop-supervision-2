import java.util.LinkedList;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class HiLo {
    public static void main(String[] args) {
        new CLIMenu();
    }
}
enum GuessResult {
    TooLow,
    TooHigh,
    Correct
}
interface SecretNumber {
    GuessResult check(int num);
}
class FairSecretNumber implements SecretNumber {
    private int number;
    public FairSecretNumber(int min, int max) {
        number = ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    @Override
    public GuessResult check(int num) {
        if (num < number) {
            return GuessResult.TooLow;
        }
        if (num > number) {
            return GuessResult.TooHigh;
        }
        return GuessResult.Correct;
    }
}

// I wasn't sure what hard meant in this game. So I added this class which doesn't actually set the number until the end.
// It makes sure that the eventual number matches all previous statements, but it is decided in the end so it takes the most guesses to find it.
// However, it is unfair so I'm not too sure about it.
class HardSecretNumber implements SecretNumber {
    private int min;
    private int max;
    public HardSecretNumber(int min, int max) {
        this.min = min;
        this.max = max;
    }
    @Override
    public GuessResult check(int num) {
        if (num < min) {
            return GuessResult.TooLow;
        }
        if (num > max) {
            return GuessResult.TooHigh;
        }
        if (min == max) {
            return  GuessResult.Correct;
        }

        if (num - min > max - num) {
            max = num - 1;
            if (max >= min) {
                return GuessResult.TooHigh;
            }
        } else {
            min = num + 1;
            if (max >= min) {
                return GuessResult.TooLow;
            }
        }
        return GuessResult.Correct;
    }
}

record GameResult(int numOfGuesses, boolean won) {}

class CLIGame {
    private int maxGuesses;
    public CLIGame(int maxGuesses) {
        this.maxGuesses = maxGuesses;
    }
    public GameResult play(SecretNumber number) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < maxGuesses; i++) {
            System.out.print("Guess " + (i + 1) + " / " + maxGuesses+ ": ");
            switch (number.check(Integer.valueOf(scanner.nextLine()))) {
                case TooLow -> System.out.println("Your guess was too low.");
                case TooHigh -> System.out.println("Your guess was too high.");
                case Correct -> {
                    System.out.println("Your guess was right!");
                    return new GameResult(i + 1, true);
                }
            }
        }
        System.out.println("I am afraid that you've lost this round.");
        return new GameResult(maxGuesses, false);
    }
}

class Scoreboard {
    private LinkedList<GameResult> results;
    public Scoreboard() {
        results = new LinkedList<>();
    }
    public void addGame(GameResult game) {
        results.add(game);
    }
    public int getTotalLost() {
        int totalLost = 0;
        for (GameResult result: results) {
            if (!result.won()) {
                totalLost++;
            }
        }
        return totalLost;
    }
    public int getTotalWon() {
        int totalLost = 0;
        for (GameResult result: results) {
            if (result.won()) {
                totalLost++;
            }
        }
        return totalLost;
    }
    public float getWinRatio() {
        return (float)getTotalWon() / (getTotalLost() + getTotalWon());
    }
    public int getWinStreak() {
        int winStreak = 0;
        for (GameResult result: results) {
            if (result.won()) {
                winStreak++;
            } else {
                winStreak = 0;
            }
        }
        return winStreak;
    }
    @Override
    public String toString() {
        return "You have won " + getTotalWon() + " / " + (getTotalLost() + getTotalWon()) + "games\n"
                + "This means you have won " + (getWinRatio() * 100) + "% of games\n"
                + "You have won the last " + getTotalWon() + " games.";
    }
}

class CLIMenu {
    public CLIMenu() {
        System.out.println("In which range do you want the numbers to be?");
        System.out.print("Min: ");
        Scanner scanner = new Scanner(System.in);
        int min = Integer.valueOf(scanner.nextLine());
        System.out.print("Max: ");
        int max = Integer.valueOf(scanner.nextLine());
        System.out.println("How many guesses do you want to have?");
        System.out.print("Number of guesses: ");
        CLIGame game = new CLIGame(Integer.valueOf(scanner.nextLine()));
        System.out.println("Do you want the game to be hard?");
        System.out.print("y/n: ");
        Scoreboard scoreboard = new Scoreboard();
        boolean hard = switch (scanner.nextLine()) {
            case "y" -> true;
            default -> false;
        };
        while (true) {
            System.out.println("I have thought of a number between " + min + " and " + max + ". Please take a guess.");
            SecretNumber number;
            if (hard) {
                number = new HardSecretNumber(min, max);
            } else {
                number = new FairSecretNumber(min, max);
            }
            scoreboard.addGame(game.play(number));
            System.out.println(scoreboard);
            System.out.println("Do you want to play another round?");
            System.out.print("y/n: ");
            boolean noNextRound = switch (scanner.nextLine()) {
                case "n" -> true;
                default -> false;
            };
            if (noNextRound) {
                break;
            }
        }
    }
}