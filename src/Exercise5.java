public class Exercise5 {
}

class Employee {
    private boolean workingFromHome;
    public Employee(boolean workingFromHome) {
        this.workingFromHome = workingFromHome;
    }
    public void work() {
        if (workingFromHome) {
            System.out.println("I am working from home!");
        } else {
            System.out.println("I am working at the office!");
        }
    }
}

class Ninja {
    public void hide() {
        System.out.println("I am hidden!");
    }
}

interface NinjaInterface {
    default void hide() {
        new Ninja().hide();
    }
}

class NinjaEmployee extends Employee implements NinjaInterface {
    public NinjaEmployee(boolean workingFromHome) {
        super(workingFromHome);
    }
}

